# Environmental Sensing: Faster, Cheaper

This is the first in a series of environmental sensing projects that live at the intersection of the following constraints:

1. **Develop open hardware and software solutions**
	There are many projects that are engaging in this space.
1. **Develop low-cost solutions**
	Many solutions in this space are *expensive*. We want solutions that are
	ultimately in the $5-$20 space.
1. **Design for manufacture**
	Many boards are designed as one-off solutions that must be hand fabricated,
	or require specialty fab that cannot be produced in volume. We will use [SparkFun's guide for selling your widget](https://learn.sparkfun.com/tutorials/how-to-sell-your-widget-on-sparkfun) as a reference.
1. **Design for battery**
	Many hobbyist solutions to sensing are cobbled together from COTS parts. They either require heavy modification, or cannot be modified, to operate for long life on battery. Our minimum target is 6 months, and we want our designs to generally last 1 year or more on 3xAA batteries.
1. **Design for modularity**
	Although Grove, Quiic, and other "modular" solutions exist for hobbyist electronics, they all fail one or more of our criteria. For example, both Grove and Quiic fail to provide any way to toggle power on the modules that are quickly assembled. Therefore, it is impossible to design for operation on battery and achieve our desired lifetimes. Our modular solution must allow for the easy (and integral) toggling of power, while also being appropriate for manufacture.
	
Yes, at the moment, this project uses Eagle, which is not open. All things with time.

Our first board is a temperature/humidity sensor based on the ESP8266, intended to be produced in quantities of 50-100 for use on a college campus. Our goal is to develop an end-to-end picture of steam generation and use, so we understand how much energy is being expended at the front end of the system, and where that energy is going through its distributive process.


